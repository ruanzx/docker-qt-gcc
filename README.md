# docker-qt-gcc

[![build status](/../badges/master/build.svg)](https://gitlab.com/csprenger/docker-qt-gcc/commits/master)

This docker image has been created to build 64 bit [Qt](https://www.qt.io)
desktop applications with GitLab's CI pipeline.
I mainly use it for continuously building and testing
[my gameboy emulator "AGE"](https://gitlab.com/csprenger/AGE).

The image is based on [Debian](https://hub.docker.com/_/debian/).
Please find prebuilt images in the [container registry](/../container_registry).
[Qt Open Source](https://download.qt.io/archive/qt/) is located at `/opt/qt`
in the docker container.
The Qt binary files' location depends on the Qt version that was installed.
`PATH` is adjusted to contain that location.

# Image Versioning

The image version is derived from the version of Qt being used.
For example all images using Qt version 5.7 have a version of "5.7.rX" where X
can be any number bigger than zero.
Please see the version table below for details on current images.

Qt and gcc version information for each image can also be found in the `test`
stage details of the [CI pipeline](/../pipelines).
The test script runs `qmake --version` and `g++ -v` during that stage.

## Version Table

All images are available in the [container registry](/../container_registry).

| image     | from           | gcc     | QMake | Qt       | Qt binaries path            |
|-----------|----------------|---------|-------|----------|-----------------------------|
| `latest`  | `stretch-slim` | `6.3.0` | `3.1` | `5.12.3` | `/opt/qt/5.12.3/gcc_64/bin` |
| `5.12.r2` | `stretch-slim` | `6.3.0` | `3.1` | `5.12.3` | `/opt/qt/5.12.3/gcc_64/bin` |
| `5.12.r1` | `stretch-slim` | `6.3.0` | `3.1` | `5.12.0` | `/opt/qt/5.12.0/gcc_64/bin` |
| `5.11.r1` | `stretch-slim` | `6.3.0` | `3.1` | `5.11.3` | `/opt/qt/5.11.3/gcc_64/bin` |
| `5.10.r1` | `stretch-slim` | `6.3.0` | `3.1` | `5.10.1` | `/opt/qt/5.10.1/gcc_64/bin` |
| `5.8.r2`  | `jessie-slim`  | `4.9.2` | `3.1` | `5.8.0`  | `/opt/qt/5.8/gcc_64/bin`    |
| `5.8.r1`  | `jessie-slim`  | `4.9.2` | `3.1` | `5.8.0`  | `/opt/qt/5.8/gcc_64/bin`    |
| `5.7.r1`  | `jessie-slim`  | `4.9.2` | `3.0` | `5.7.1`  | `/opt/qt/5.7/gcc_64/bin`    |
