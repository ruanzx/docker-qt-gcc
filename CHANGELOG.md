
## 5.12.r2

- Use Qt version 5.12.3.

## 5.12.r1

- Use Qt version 5.12.0.

## 5.11.r1

- Use Qt version 5.11.3.

## 5.10.r1

- Use [Debian 9 ("stretch")](https://www.debian.org/releases/stretch/)
  as base image.
- Upgrade to GCC 6.3.0 (from 4.3.0).
- Use Qt version 5.10.1.

## 5.8.r2

- Removed `pulseaudio`.
- Added `libpulse-dev`.

## 5.8.r1

- Raised Qt version to 5.8.

## 5.7.r1

- first release with versioning derived from installed Qt version

## 1.0

- first release
